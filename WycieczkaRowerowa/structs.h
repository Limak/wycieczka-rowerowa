#pragma once

struct Person
{
	int distance;
	int amountOfChoosenBicycles;
	int* choosenBicycles;
};

struct BipGraph
{
	int amountOfPeople;
	int amountOfBicycles;
	int* pairPeople;
	int* pairBicycles;
	int* dist;
	Person* people;
};
