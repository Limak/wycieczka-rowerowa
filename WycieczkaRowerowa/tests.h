#pragma once
#include "constValues.h"
#include "structs.h"
#include "WycieczkaRowerowa.h"
#include "HopcroftKarp.h"

#include <stdio.h>

void testAmountOfBicyclesForEachPerson(BipGraph* graph);
void testChoosenBicycles(BipGraph& graph);
