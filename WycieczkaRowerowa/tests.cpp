#include "tests.h"

void testAmountOfBicyclesForEachPerson(BipGraph * graph)
{
	int amountOfPeople = graph->amountOfPeople;
	for (int i = 1; i <= amountOfPeople; i++)
	{
		int amountOfBicycles = graph->people[i].amountOfChoosenBicycles;
		printf("osoba:%d ilosc rowerow:%d\n", i, amountOfBicycles);
	}
}

void testChoosenBicycles(BipGraph& graph)
{
	int amountOfPeople = graph.amountOfPeople;
	for (int i = 1; i <= amountOfPeople; i++)
	{
		int amountOfBicycles = graph.people[i].amountOfChoosenBicycles;
		printf("osoba:%d ilosc rowerow:%d	wybrane rowery: ", i, amountOfBicycles);
		for (int j = 1; j <= amountOfBicycles; j++)
		{
			printf("%d ", graph.people[i].choosenBicycles[j]);
		}
		printf("\n");
	}
}
