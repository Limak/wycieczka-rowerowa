#pragma once
#include "structs.h"
#include  "constValues.h"
#include "WycieczkaRowerowa.h"

void giveBeginValueForPairs(BipGraph* graph);
bool BFS(BipGraph* graph);
bool DFS(BipGraph* graph, int person);
int findMatching(BipGraph* graph);