
//#include <stdio.h>
#include <cstdio>
#include "WycieczkaRowerowa.h"
#include "structs.h"
#include "HopcroftKarp.h"
#include "tests.h"

void readInput(BipGraph * graph)
{
}

void mainProgram()
{
	BipGraph graph;
	graph.amountOfPeople = 0;
	graph.amountOfBicycles = 0;

	///--------------------wczytywanie i tworzenie tablic dla ludzi i rowerow------------------------------------------
	int amountOfBicycles = 0;
	int amountOfPeople = 0;
	scanf("%d %d", &amountOfPeople, &amountOfBicycles);
	graph.amountOfPeople = amountOfPeople;
	graph.amountOfBicycles = amountOfBicycles;

	graph.people = new Person[graph.amountOfPeople + 1];	//bo index 0 jest indexem NIL (specjalnym)

	// wczytanie kazdej osobie wybranych przez nia rowerow
	for (int i = 1; i <= graph.amountOfPeople; i++)
	{
		int amountOfChoosenBicyclesForPerson = 0;
		scanf("%d", &amountOfChoosenBicyclesForPerson);
		graph.people[i].amountOfChoosenBicycles = amountOfChoosenBicyclesForPerson;

		graph.people[i].choosenBicycles = new int[amountOfChoosenBicyclesForPerson + 1];

		for (int j = 1; j <= amountOfChoosenBicyclesForPerson; j++)
		{
			int choosenBicycle = 0;
			scanf("%d", &choosenBicycle);
			graph.people[i].choosenBicycles[j] = choosenBicycle;
		}
	}


	//testAmountOfBicyclesForEachPerson(&graph);
	//testChoosenBicycles(graph);

	/// przypisanie poczotkowych wartosci wierzcholkom grafu
	graph.pairBicycles = new int[graph.amountOfBicycles + 1];	//poniewaz rowery lica sie od 1
	graph.pairPeople = new int[graph.amountOfPeople + 1];;
	graph.dist = new int[graph.amountOfBicycles + 1];


	
	
	///poczotkowe wartosci dla kazdego wezla 
	giveBeginValueForPairs(&graph);

	int result = findMatching(&graph);
	printf("%d", result);


	//delete[] graph.people;
	//delete[] graph.people->choosenBicycles;	//TODO usuwac pamiec
	//delete[] graph.pairBicycles;
	//delete[] graph.pairPeople;
}

int main()
{
	mainProgram();
	return 0;
}


