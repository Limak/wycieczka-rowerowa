#include "HopcroftKarp.h"
#include <queue>
#include "constValues.h"
#include "tests.h"
#include "WycieczkaRowerowa.h"

#include <limits.h>


void giveBeginValueForPairs(BipGraph * graph)
{
	for (int i = 0; i <= graph->amountOfBicycles; i++)
	{
		graph->pairBicycles[i] = NIL;
	}

	for (int i = 0; i <= graph->amountOfPeople; i++)
	{
		graph->pairPeople[i] = NIL;
	}
}



bool BFS(BipGraph* graph)
{
	//testChoosenBicycles(*graph);
	std::queue<int> Q;	//kolejka z indexami osob
	int amountOfPeople = graph->amountOfPeople;
	for (int u = 1; u <= amountOfPeople; u++)
	{
		if (graph->pairPeople[u] == NIL)
		{
			//graph->people[u].distance = 0;
			graph->dist[u] = 0;
			Q.push(u);
		}
		else
			//graph->people[u].distance = INT_MAX;
			graph->dist[u] = INT_MAX;
	}

	//graph->people[NIL].distance = INT_MAX;
	graph->dist[NIL] = INT_MAX;

	while (!Q.empty())
	{
		int firstPersonDistInQueue = Q.front();
		/*int tempDist = graph->people[firstPersonDistInQueue].distance;
		int distNil = graph->people[NIL].distance;
		int maxInt = INFINITY;*/
		Q.pop();
		if (graph->dist[firstPersonDistInQueue] < graph->dist[NIL])
		{
			int testAmountOfBicyclesForPerson = graph->dist[firstPersonDistInQueue];
			for (int v = 1; v <= graph->people[firstPersonDistInQueue].amountOfChoosenBicycles; v++)
			{
				int bicycleDistance = graph->pairBicycles[v];
				if (graph->dist[bicycleDistance] == INT_MAX)
				{
					graph->dist[bicycleDistance] = graph->dist[firstPersonDistInQueue] + 1;
					Q.push(bicycleDistance);
				}
			}
		}
	}

	return (graph->dist[NIL] != INT_MAX);
}

bool DFS(BipGraph * graph, int person)
{
	if (person != NIL)
	{
		for (int v = 1; v <= graph->people[person].amountOfChoosenBicycles; v++)
		{
			//TODO dodac kolejnego ifa
			int pairBicycles = graph->pairBicycles[v];
			if (graph->dist[pairBicycles] == graph->dist[person] + 1)
			{
				if (DFS(graph, pairBicycles))
				{
					graph->pairBicycles[v] = person;
					graph->pairPeople[person] = v;
					return true;
				}
			}
			graph->dist[person] = INT_MAX;
			return false;
		}
	}

	return true;
}


int findMatching(BipGraph* graph)
{
	int matching = 0;
	//testChoosenBicycles(*graph);

	while (BFS(graph))
	{
		for (int u = 1; u <= graph->amountOfPeople; u++)
		{
			if (graph->pairPeople[u] == NIL)
			{
				if (DFS(graph, u))
					matching += 1;
			}
		}
	}
	return matching;
}
